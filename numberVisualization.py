from numpy import left_shift
import pygame
from numberDetection import *
import matplotlib.pyplot as plt
import random

n = runCamera()

pygame.init()
screen = pygame.display.set_mode((1280,720))
pygame.display.set_caption('Visualize')
run = True

dotsN = 0 #For dots UI
visN = 0 #For type of visualizations

colours = []
for i in range(100):
    #Creates 100 random colours for the bar chart/dots
    colours.append((random.randint(0,255),random.randint(0,255),random.randint(0,255)))

#Set up fonts and text
nFont = pygame.font.Font('freesansbold.ttf', 25)
xAxisFont = pygame.font.Font('freesansbold.ttf', 30)

#Arrows for the UI
rightArrow = pygame.image.load("UIArrow.png")
leftArrow = pygame.transform.flip(rightArrow, True, False) #2nd parameter = vertical flip, 3rd = horizontal
rightArrow = pygame.transform.scale(rightArrow, (50,50))
leftArrow = pygame.transform.scale(leftArrow, (50,50))
rightChangeArrow = pygame.transform.scale(rightArrow, (75, 75))
leftChangeArrow = pygame.transform.scale(leftArrow, (75, 75))

def mousePressed(x, y, width, height):
    if pygame.mouse.get_pos()[0] > x and pygame.mouse.get_pos()[0] < x+width: #Checks if x is in between the left and the right of the object
        if pygame.mouse.get_pos()[1] > y and pygame.mouse.get_pos()[1] < y+height: #Checks if y is in between the top and the bottom of the object
            return True
        else:
            return False
    else:
        return False

def drawDots(n):

    global dotsN

    dotsText = nFont.render("Number: " + str(n[dotsN]), True, (0,0,0))
    screen.blit(dotsText, (75,20))
    screen.blit(leftArrow, (0, 10))
    screen.blit(rightArrow, (250, 10))

    sqr = n[dotsN] ** 0.5
    y = 0
    for i in range(n[dotsN]):
        if i-(y*int(sqr)) > sqr:
            y += 1
        pygame.draw.circle(screen, colours[dotsN], ((i*40)+60-(y*int(sqr)*40), 50*y + 200), 20)


def drawBarGraph(n):
    pygame.draw.line(screen, (0,0,0), (100,50), (100,650), 5) #y-axis
    pygame.draw.line(screen, (0,0,0), (100,650), (1100,650), 5) #x-axis

    #bars
    numberOfBars = len(n)
    maxHeight = max(n)

    barWidth = int(1000/numberOfBars)

    for i in range(numberOfBars):
        pygame.draw.rect(screen, (0,0,0), ((i*barWidth)+110, int(650-((n[i]/maxHeight)*600)), barWidth*0.8, int((n[i]/maxHeight)*600)))
        #Text under bars
        xAxisText = xAxisFont.render(str(n[i]) , True, (0,0,0))
        screen.blit(xAxisText, ((i*barWidth)+(0.5*barWidth)+70,670))


def drawPieChart(n):
    fig = plt.figure()
    fig.patch.set_facecolor([0.9294,0.921,0.9137]) #Numbers are in RGB and are divided by 255
    plt.pie(n, labels = n)
    plt.savefig('pieChart.jpg') #Saves image so chart doesn't appear on another window
    pieChart = pygame.image.load("pieChart.jpg")
    pieChart = pygame.transform.scale(pieChart, (960,720)) #Scales it so it's bigger
    screen.blit(pieChart, (160, 0))

while run:
    screen.fill((237, 235, 233))
    try:
        if visN == 0:
            drawDots(n)
        elif visN == 1:
            drawBarGraph(n)
        elif visN == 2: 
            drawPieChart(n)
    except:
        errorText = nFont.render("Error, no numbers found in picture!", True, (0,0,0))
        screen.blit(errorText, (400, 340))
        pygame.display.update()
        runCamera()   

    #Arrows to change visualization type
    screen.blit(leftChangeArrow, (10, 630))
    screen.blit(rightChangeArrow, (1200, 630))

    #Button to take picture again
    pygame.draw.rect(screen, (200,255,200), (1050, 10, 180, 40))
    pictureText = nFont.render("Take picture", True, (0,0,0))
    screen.blit(pictureText, (1060, 15))

    pygame.display.update()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        if event.type == pygame.MOUSEBUTTONDOWN:
                #Dots UI
                if mousePressed(0,10,50,50):
                    dotsN -= 1
                    if dotsN < 0:
                        dotsN = len(n)-1

                if mousePressed(250,10,50,50):
                    dotsN += 1
                    if dotsN > len(n)-1:
                        dotsN = 0

                if mousePressed(10,630,75,75):
                    visN -= 1
                    if visN < 0:
                        visN = 2

                if mousePressed(1200,620,75,75):
                    visN += 1
                    if visN > 2:
                        visN = 0

                if mousePressed(1050, 10, 180, 40):
                    n = runCamera()

            
