from PIL import Image
import time
import climage

txt1 = "Fractions can sometimes be quite scary, but don’t worry. I am here to help you understand them as simply as possible."
txt2 = "Lets think about fractions in terms of a pizza. Yummy!!!\nRight now, we have a whole pizza with us."
txt3 = "Now lets say we have a friend who wants to share the pizza, now we have to cut it into two pieces.\n\n\n"
txt4 = "Our one whole pizza has now been split into two smaller pizza slices.\n\nWe have split our pizza into half!"
txt5 = "Lets say two more friends come along and want a slice."
txt6 = "Now we have to cut our two slices into halfs again to make 4 slices"
txt7 = "We have now cut our pieces into QUARTERS"

output = climage.convert('Fractions tutorial\pizza_whole1.png')
output1 = climage.convert('Fractions tutorial\pizza_half_left1.png')
output2 = climage.convert('Fractions tutorial\pizza_half_right1.png')
output3 = climage.convert('Fractions tutorial\pizza_quarter_top_left1.png')
output4 = climage.convert('Fractions tutorial\pizza_quarter_top_right1.png')
output5 = climage.convert('Fractions tutorial\pizza_quarter_bottom_left1.png')
output6 = climage.convert('Fractions tutorial\pizza_quarter_bottom_right1.png')

print(txt1)
time.sleep(4)
print(txt2)
time.sleep(4)
print(output)
time.sleep(4)
print(txt3)
time.sleep(4)
print(txt4)
time.sleep(4)
print(output)
print(output1, "-----------------------------------", output2)
time.sleep(4)
print(txt5)
time.sleep(4)
print(txt6)
time.sleep(4)
print(txt7)
time.sleep(4)
print(output)
print(output1, "  ", output2)
print(output3, "  ", output4)
print(output5, "  ", output6)