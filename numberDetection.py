#10/8/2021
#Calculi

import cv2
import easyocr
from numpy import number #Identifies text in an imgae

numbersToCheck = "0 1 2 3 4 5 6 7 8 9".split() #Creates array with all numbers

def checkNumber(data, frame):
    numbers = []
    for i in range(len(data)):
        l = data[i][-1].split()
        for j in range(len(l)):
            try:
                numbers.append(int(l[j]))
                frame = cv2.rectangle(frame, data[i][0][0], data[i][0][2], (0,0,255), 1) #Colour is in BGR not RGB 
                cv2.imwrite('userTakenImage.png', frame)
            except ValueError:
                pass

    return numbers

def runCamera():
    cap = cv2.VideoCapture(0) 
    while(True): 
        _, frame = cap.read() #_ = throwaway variable (just returns true if frame is available)

        cv2.imshow("preview", frame)
        if cv2.waitKey(1) == 113: #ASCII for "q" 
            break
        elif cv2.waitKey(1) == 32: #ASCII for space
            reader = easyocr.Reader(['en'])
            result = reader.readtext(frame ,paragraph="True")
            cv2.destroyAllWindows()
            return checkNumber(result, frame)

    cv2.destroyAllWindows()
