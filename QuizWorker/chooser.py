import random
import pprint

def pick_ten(fname):
    question_list = []
    for question in open(fname):
        # strip off new line character
        question = question.rstrip()
        question_list.append(question)
    return random.sample(question_list, 5)

x = 1
filename = "QuizWorker\level1.txt"

while x == 1:
    # testing ...
    question_list = pick_ten(filename)
    pprint.pprint(question_list)

    answer1 = input("1) ")
    answer2 = input("2) ")
    answer3 = input("3) ")
    answer4 = input("4) ")
    answer5 = input("5) ")
